#PHPCrazy.Class.Zip
PHPCrazy Zip 类

##如何使用？

下载 [ Crazy / PHPCrazy.Class.Zip](http://git.oschina.net/Crazy-code/PHPCrazy.Class.Zip/repository/archive/master)，解压到PHPCrazy的根目录下：

##实例

###
```php
<?php

$zip = new Zip();

$DirName = 'test';

$ZipFile = 'test.zip';

$SaveDir = 'savedir';

// 压缩服务器文件/文件夹并下载
//$zip->ZipAndDownload($DirName, 'name');

// 压缩服务器文件/文件夹并保持在服务器上
//$zip->Zip($DirName, $SaveDir.'/name.zip');

// 从服务器解压zip文件到服务器
$zip->unZip($ZipFile, $SaveDir);

?>
```